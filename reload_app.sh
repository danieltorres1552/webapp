#!/bin/zsh
#Carga la variable de entorno catalina y los ejecutables para apagar y inicar el servidor.
source ~/bin/tomcat/handler_tomcatserver.sh

#Nombre del binario es una constante. 
constela-javaweb-app.war

mvn clean package
cp target/constela-javaweb-app.war /usr/local/apache-tomcat-8.5.75/webapps/

shutdown_tomcat && startup_tomcat


