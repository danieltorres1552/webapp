package com.constela.persistence.controller;

import com.constela.persistence.ProductDataAccess;
import com.constela.persistence.model.Product;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

public class CtrlProduct implements Serializable {

    //Instanciarlos desde el contructor.
    private ProductDataAccess services;
    private List<Product> products;

    public CtrlProduct() {
        this.services = new ProductDataAccess();
    }

    public void saveProduct(Product product) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Entro al controlador"));
        services.save(product);
    }

    public List<Product> getProducts() {
        return services.getProducts();
    }

    public Product getProductById(int id) {
        return services.getProductById(id);
    }

    public void updateProduct(int id, Product product) {
        services.updateProduct(id, product);
    }

    public void deleteProduct(int id) {
        services.deleteProduct(id);
    }
}
