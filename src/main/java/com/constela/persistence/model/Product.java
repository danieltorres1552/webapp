package com.constela.persistence.model;

public class Product {
  private int id;
  private String name;
  
  public Product(){
  }

  public Product(String name){
     this.name = name;
  }
  
  public Product(int id, String name){
     this.id = id;
     this.name = name;
  }

  public void setId(int id){
    this.id = id;
  }

  public int getId(){
    return this.id;
  }

  public void setName(String name){
    this.name = name;
  }

  public String getName(){
    return this.name;
  }
  
}
