/**
 * Copyright (c) Microsoft Corporation. All rights reserved.
 * Licensed under the MIT License. See LICENSE in the project root for
 * license information.
 */
package com.constela.user;

import java.util.ArrayList;
import javax.enterprise.context.ApplicationScoped;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;
import javax.annotation.PostConstruct;
import javax.inject.Named;

@Named
@ApplicationScoped
public class TodoItemManagementInMemory implements ItemManagement {

    private CopyOnWriteArrayList<TodoItem> todoItems = new CopyOnWriteArrayList<TodoItem>();

    private List<TodoItem> items;

    @PostConstruct
    public void init() {
        items = new ArrayList<>();
        items.add(new TodoItem(convertLong("1"), "Taza", "Casa/Comedor2", "56-54-34-54-34", true));
        items.add(new TodoItem(convertLong("2"), "Taza", "Casa/Comedor2", "56-54-34-54-34", true));
        items.add(new TodoItem(convertLong("3"), "Taza", "Casa/Comedor2", "56-54-34-54-34", true));
        items.add(new TodoItem(convertLong("4"), "Taza", "Casa/Comedor2", "56-54-34-54-34", true));
        items.add(new TodoItem(convertLong("5"), "Taza", "Casa/Comedor2", "56-54-34-54-34", true));
        items.add(new TodoItem(convertLong("6"), "Taza", "Casa/Comedor2", "56-54-34-54-34", true));
        items.add(new TodoItem(convertLong("7"), "Taza", "Casa/Comedor2", "56-54-34-54-34", true));
        items.add(new TodoItem(convertLong("8"), "Taza", "Casa/Comedor2", "56-54-34-54-34", true));
        items.add(new TodoItem(convertLong("9"), "Taza", "Casa/Comedor2", "56-54-34-54-34", true));
        items.add(new TodoItem(convertLong("10"), "Taza", "Casa/Comedo2r", "56-54-34-54-34", true));
        items.add(new TodoItem(convertLong("11"), "Taza", "Casa/Comedor2", "56-54-34-54-34", true));
        items.add(new TodoItem(convertLong("12"), "Taza", "Casa/Comedor2", "56-54-34-54-34", true));
        items.add(new TodoItem(convertLong("13"), "Taza", "Casa/Comedor2", "56-54-34-54-34", true));
        items.add(new TodoItem(convertLong("14"), "Taza", "Casa/Comedor2", "56-54-34-54-34", true));
        items.add(new TodoItem(convertLong("15"), "Taza", "Casa/Comedor2", "56-54-34-54-34", true));
        items.add(new TodoItem(convertLong("16"), "Taza", "Casa/Comedor2", "56-54-34-54-34", true));
    }

    //Llega un entero devuelve un long.
    public Long convertLong(String stringNum) {
        return Long.parseLong(stringNum);
    }

    public CopyOnWriteArrayList<TodoItem> getTodoItems() {
        return todoItems;
    }

    public void setTodoItems(CopyOnWriteArrayList<TodoItem> todoItems) {
        this.todoItems = todoItems;
    }

    public TodoItem getItemById(int id) {
        return todoItems.get(id);
    }

    public void addTodoItem(TodoItem item) {
        synchronized (this) {
            int size = todoItems.size();
            long id = 1;
            if (size != 0) {
                id = todoItems.get(size - 1).getIdUser();
                id++;
            }
            item.setIdUser(id);
            todoItems.add(item);
        }
    }

    public void updateTodoItem(List<TodoItem> items) {
        items.stream().forEach(item -> {
            item.setComplete(true);
            synchronized (this) {
                todoItems.set(item.getIdUser().intValue(), item);
            }
        });
    }

    public void deleteAlltem() {
        synchronized (this) {
            todoItems.retainAll(todoItems);
        }
    }

    public void removeTodoItem(int id) {
        synchronized (this) {
            id--;
            //int size = todoItems.size();
            todoItems.remove(id);
        }
    }

    public void updateTodoItem(int idUser) {
        synchronized (this) {

        }
    }

    public List<TodoItem> getItems() {
        return new ArrayList<>(items);
    }
    
    

    List<TodoItem> getItems(int size) {
        if (size > items.size()) {
            Random rand = new Random();

            List<TodoItem> randomList = new ArrayList<>();
            for (int i = 0; i < size; i++) {
                int randomIndex = rand.nextInt(items.size());
                randomList.add(items.get(randomIndex));
            }

            return randomList;
        } else {
            return new ArrayList<>(items.subList(0, size));
        }
    }

}
