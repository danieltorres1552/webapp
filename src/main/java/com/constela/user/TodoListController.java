/**
 * Copyright (c) Microsoft Corporation. All rights reserved.
 * Licensed under the MIT License. See LICENSE in the project root for
 * license information.
 */
package com.constela.user;

import com.constela.user.TodoItemManagementInMemory;
import com.constela.persistence.controller.CtrlProduct;
import com.constela.persistence.model.Product;

import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.PrimeFaces;

@Named("productView")
@ViewScoped
public class TodoListController implements Serializable {

    private static final long serialVersionUID = 1L;
    private Product selectedProduct;

    @Inject
    TodoItemManagementInMemory todoManagement;

    private List<TodoItem> todoItems;

    //Este objeto vivira a lo largo del tiempo de vida de la aplicacion.
    private List<TodoItem> sampleItems;

    private TodoItem selectedItem;
    private List<TodoItem> selectedItems;

    private List<Product> products;

    @Inject
    private CtrlProduct ctrlProduct;

    private Long idUser;
    private String name;
    private String category;
    private String telefono;

    //Se ejecutara depues de que se levante la aplicacion.
//    @PostConstruct
//    public void init() {
//        System.out.println("fFFFFFFFFFFFSSSSSSSFFFFFFSDGSDHSDFGSDFG");
//        //Llenara un arreglo de objetos.
//        todoItems = service.getItems();
//        todoItems.forEach(item -> {
//            System.out.println(item);
//        });
//
//    }
    public List<Product> getTodoItems() {
        products = ctrlProduct.getProducts();
        return products;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public void buttonUpdateAction() {
        todoManagement.updateTodoItem(selectedItems);
    }

    public void buttonAddAction() {
        System.out.print(idUser);
        Long longId = Long.valueOf(idUser);
        TodoItem addItem = new TodoItem(longId, name, category, telefono, false);
        todoManagement.addTodoItem(addItem);
    }

    /**
     * Elimina un elemento usuario.
     *
     * @param idUser Identificador unico del usuario.
     *
     */
    public void buttonRemoveAction(int idUser) {
        System.out.print(idUser);
        todoManagement.removeTodoItem(idUser);
    }

    /**
     * Actualiza un elemento usuario.
     *
     * @param idUser Identificador unico del usuario.
     *
     */
    public void buttonUpdateAction(int idUser) {
        TodoItem item = todoManagement.getItemById(idUser - 1);

        todoManagement.updateTodoItem(idUser);
    }

    public void setSelectedItem(TodoItem selectedItem) {
        this.selectedItem = selectedItem;
    }

    public List<TodoItem> getSelectedItem() {
        return selectedItems;
    }

    public void setSelectedItems(List<TodoItem> selectedItems) {
        this.selectedItems = selectedItems;
    }

    public List<TodoItem> getSelectedItems() {
        return selectedItems;
    }

    public void onRowSelect(SelectEvent<TodoItem> event) {
        FacesMessage msg = new FacesMessage("TodoItem Selected", event.getObject().getIdUser().toString());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowUnselect(UnselectEvent<TodoItem> event) {
        FacesMessage msg = new FacesMessage("Car Unselected", event.getObject().getIdUser().toString());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    List<TodoItem> getItems() {
        return todoItems;
    }
    
    /**
     * Esta funcion es un upsert
     * */
    public void saveProduct() {
        System.out.println(this.selectedProduct);
        if (this.selectedProduct.getId() != 0) {
//            this.selectedProduct.setCode(UUID.randomUUID().toString().replaceAll("-", "").substring(0, 9));
//            this.products.add(this.selectedProduct);
            this.ctrlProduct.updateProduct(this.selectedProduct.getId(), this.selectedProduct);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Product Actualizado"));
            
        } else {
            System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            System.out.println("------------------------------------------Nuevo producto registrdo ----------------------------------------------------------------");
            System.out.println(this.selectedProduct);
            System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            this.ctrlProduct.saveProduct(this.selectedProduct);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Product Agregado"));
        }

        PrimeFaces.current().executeScript("PF('manageProductDialog').hide()");
        PrimeFaces.current().ajax().update("form:messages", "form:dt-products");
    }

    //Function
    public Product getSelectedProduct() {
        return selectedProduct;
    }

    public void setSelectedProduct(Product selectedProduct) {
        this.selectedProduct = selectedProduct;
    }

    public void openNew() {
        this.selectedProduct = new Product();
    }

    //Messages
    public void deleteProduct() {
        System.out.println(this.selectedProduct);
        //Este metodo espera recibir un objeto el mio un id.
        this.ctrlProduct.deleteProduct(this.selectedProduct.getId());
        this.selectedProduct = null;
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Product Eliminado"));
        PrimeFaces.current().ajax().update("form:messages", "form:dt-products");
    }

}
